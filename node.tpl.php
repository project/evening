<div class="node<?php if ($sticky) { print " sticky"; } ?><?php if (!$status) { print " node-unpublished"; } ?>">
<?php if ($page == 0) { ?><div class="nodeTitle<?php if (!$picture) {print ' nobreak';}?>"><a href="<?php print $node_url?>"><?php print $title?></a></div><?php }; ?>
<div id="nwrap">
  <div class="post-date"><span class="post-month"><?php print (format_date($node->created, 'custom', 'M')) ?></span> <span class="post-day"><?php print (format_date($node->created, 'custom', 'd')) ?></span></div>
	<span class="submitted">Submiter: <?php print $name ?>	</span><br/><span class="linkswrap">
	<?php if ($links) { ?><span class="postlinks"><?php print $links?></span><?php }; ?>
	<?php if ($terms) { ?><span class="taxonomy"><?php print $terms?></span><?php } ?>
	</span>
</div>
  <div class="content">
  <?php if ($picture) { ?>
    <?php if ($page == 0) { print '<div class="clearpic"></div>'; } ?><?php print $picture; ?>
  <?php } ?>
  <?php print $content?></div>
  


</div>
