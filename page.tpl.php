<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" lang="<?php print $language ?>" xml:lang="<?php print $language ?>">

<head>
  <title><?php print $head_title ?></title>
  <?php print $head ?>
  <?php print $styles ?>
  <?php print $scripts ?>
</head>

<body>
  <div id="header">
    <div id="headerInner">
      <div id="logowrapper">
        <?php if ($logo) { ?><div id="logo">
            <a href="<?php print $base_path ?>" title="<?php print t('mydrupal.com') ?>"><img src="<?php print $logo ?>" alt="" /></a>
          </div><?php } ?>
        <?php if ($site_name) { ?><div id="siteName">
            <a href="<?php print $base_path ?>" title="<?php print t('Home') ?>"><?php print $site_name ?></a>
          </div><?php } ?>
        <?php if ($site_slogan) { ?><div id="siteSlogan"><?php print $site_slogan ?></div><?php } ?>
      </div>
      <div id="navigation">
        <?php print theme('menu_links', $primary_links) ?> 
      </div>	
    </div>
  </div>

  <!-- main -->
  <div id="main">
    <div id="mainInner">
      <div id="primaryContent">
        <div id="columns" style="width: <?php print alek_2_0_get_primaryContent_width( $sidebar_left, $sidebar_right) ?>%;">
          <?php if ($breadcrumb) { ?><div class="breadcrumb"><?php print $breadcrumb ?></div><?php } ?>

			  <?php if ($mission) { ?><div id="mission"><?php print $mission ?></div><?php } ?>		  
          <?php if ($title) { ?><h1 class="pageTitle"><?php print $title ?></h1><?php } ?>
          <?php if ($tabs) { ?><div class="tabs"><?php print $tabs ?></div><?php } ?>
          <?php if ($help) { ?><div class="help"><?php print $help ?></div><?php } ?>
          <?php if ($messages) { ?><div class="messages"><?php print $messages ?></div><?php } ?>
          <?php print $content ?>
        </div>
      </div>
      <!-- sidebars -->
      <div id="secondaryContent">
        <?php if ($sidebar_left) { ?>
		      <div id="sidebarLeft">
			      <span id="topspan"></span>
            <div class="sidebarLeft-content">
				      <?php print $sidebar_left ?>
				    </div>
			      <span id="bottomspan"></span>
		      </div>
		    <?php } ?>
        <?php if ($sidebar_right) { ?>
          <div id="sidebarRight">
            <?php print $sidebar_right ?>
          </div>
        <?php } ?>
      </div>
    </div>
  </div>

  <!-- footer -->
  <div class="FooterContain">
    <div class="Footer">
      <div class="FooterRecent">
        <?php print $footer_left; ?>
      </div>
      <div class="FooterCommented">
        <?php print $footer_middle; ?>
      </div>
      <div class="FooterPartners">
        <?php print $footer_right; ?>
      </div>

      <div class="FooterCopy">
	      <p>
          <?php if ($footer_message) { ?> <?php print $footer_message ?><?php } ?>
        </p>
	      <div class="credit">
	        <p><a href="http://mydrupal.com">Drupal Themes</a> by MyDrupal.com. Sponsored by: <a href="http://pmzilla.com">PMP</a> | <a href="http://itdiscover.com">IT Discover</a> | <a href="http://techjobs.co.in">Techjobs</a>. Inspired by: <a href="http://www.justinshattuck.com/">Salmon</a>
			
			</p>
	      </div>
      </div>
    </div>
  </div>
  <?php print $closure ?>
</body>
</html>
